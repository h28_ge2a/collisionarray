#pragma once

#include "MyGameEngine\Fbx.h"


//配列は関数でやり取りできないので無駄に構造体にする
struct StageData
{
	//衝突判定用配列
	BOOL collisionTable[10][10];
};



//ステージクラス
class Stage:public Fbx
{
	
	StageData _data;


public:
	Stage();
	~Stage();


	void init();

	CREATE_FUNC(Stage);

	//データを渡す
	StageData getData(){ return _data; }
};

