#pragma once
#include "MyGameEngine\Fbx.h"
#include "Stage.h"

class Ball : public Fbx
{
	D3DXVECTOR3 _move;


public:
	Ball();
	~Ball();



	void init();
	void input() override;

	CREATE_FUNC(Ball);

	void collision(StageData data);
};

