#pragma once
#include "MyGameEngine\Scene.h"

class Stage;
class Ball;
class PlayScene :public Scene
{
	Stage*	_stage;
	Ball*	_ball;

public:
	PlayScene();
	~PlayScene();
	void init()   override;
	void update() override;
	void input() override;
};

