#include "Ball.h"


Ball::Ball()
{
}


Ball::~Ball()
{
}

void Ball::init()
{
	load("Assets\\ball.fbx");
}

void Ball::input()
{
	Fbx::input();



	//移動ベクトル
	_move = D3DXVECTOR3(0, 0, 0);

	if (g.pInput->isKeyPush(DIK_RIGHT))
	{
		_move.x += 0.1f;
	}

	if (g.pInput->isKeyPush(DIK_LEFT))
	{
		_move.x -= 0.1f;
	}

	if (g.pInput->isKeyPush(DIK_UP))
	{
		_move.z += 0.1f;
	}

	if (g.pInput->isKeyPush(DIK_DOWN))
	{
		_move.z -= 0.1f;
	}


	//移動
	_position += _move;
}


//ステージとの衝突判定
void Ball::collision(StageData data)
{
	D3DXVECTOR3 normalMove;
	D3DXVec3Normalize(&normalMove, &_move);
	D3DXVECTOR3 checkPoint = _position + normalMove * 0.5f;	//ボールの中心から進行方向に半径分ずらしたところをチェック

	//そこが壁だったら
	if (data.collisionTable[(int)(checkPoint.x + 5)][9 - (int)(checkPoint.z + 5)] == 1)
	{
		//とりあえず戻す
		_position -= _move;
	}
}