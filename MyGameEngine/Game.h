#pragma once
#include "Global.h"
#include "Scene.h"

class Game
{
	LPDIRECT3D9	_pD3d;	   //Direct3Dオブジェクト

	
public:
	Game();					//コンストラクタ
	~Game();				//デストラクタ（開放処理）
	void init(HWND Hwnd);	//初期化処理
	void update();			//更新処理
	void input();			//入力処理
	void draw();			//描画処理
};