#include "Scene.h"
#include "Camera.h"

Scene::Scene()
{
	auto camera = Camera::create();
	this->addChild(camera);
	_camera = camera;
}


Scene::~Scene()
{
	for (int i=0; i < _nodes.size(); i++)
	{
		SAFE_DELETE	(_nodes[i]);
	}
}

void Scene::addChild(Node* pNode)
{
	pNode->setParent(this);
	_nodes.push_back(pNode);
}

void Scene::update()
{
	for (int i = 0; i < _nodes.size(); i++)
	{
		_nodes[i]->update();
	}
}

void Scene::input()
{
	for (int i = 0; i < _nodes.size(); i++)
	{
		_nodes[i]->input();
	}
}


void Scene::draw()
{
	for (int i = 0; i < _nodes.size(); i++)
	{
		_nodes[i]->draw(); 	
		}
}

//ノードの削除関数
void Scene::removeChild(Node* pNode)
{
	for (int i = 0; i < _nodes.size(); i++)
	{
		if (pNode == _nodes[i])
		{
			SAFE_DELETE(_nodes[i]);
			_nodes.erase(_nodes.begin() + i);
			break;
		}
	}
}