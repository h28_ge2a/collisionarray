#include "PlayScene.h"

#include "MyGameEngine\Fbx.h"
#include "MyGameEngine\Light.h"
#include "MyGameEngine\Camera.h"
#include "Stage.h"
#include "Ball.h"

PlayScene::PlayScene()
{

}


PlayScene::~PlayScene()
{

}

void PlayScene::init()
{
	_camera->setPosition(0, 8, -5);

	auto light = Light::create();
	this->addChild(light);

	auto stage = Stage::create();
	this->addChild(stage);
	_stage = stage;

	auto ball = Ball::create();
	this->addChild(ball);
	_ball = ball;


}


void PlayScene::update()
{
	Scene::update();
	_ball->collision(_stage->getData());


}

void PlayScene::input()
{
	Scene::input();

}